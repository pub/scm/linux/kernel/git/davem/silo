#ifndef _NONLINUX_TYPES_H
#define _NONLINUX_TYPES_H
typedef signed char	__s8;
typedef unsigned char	__u8;
typedef signed short	__s16;
typedef unsigned short	__u16;
typedef signed int	__s32;
typedef unsigned int	__u32;
#endif
