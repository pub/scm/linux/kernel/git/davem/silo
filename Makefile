include Rules.make


# These only get built on Linux
ifeq ($(OPSYS),Linux)
ifneq ($(TILO_ONLY),yes)
  SUBDIRS  = common first second first-isofs
endif
  SUBDIRS += tilo
  MANPAGES = maketilo.1 tilo.1
endif

ifneq ($(TILO_ONLY),yes)
# These get built on Linux and Solaris
MANPAGES += silo.8 silo.conf.5
SUBDIRS  += silo
endif

all dep depend clean:
	@for I in $(SUBDIRS); do $(MAKE) -C $$I $@ || exit 1; done

ifeq ($(OPSYS),$(findstring $(OPSYS),Linux Solaris))
install:
ifneq ($(TILO_ONLY),yes)
	install -d -m755 $(DESTDIR)/etc $(DESTDIR)/sbin $(DESTDIR)/usr/sbin
	install -m755 silo/silo $(DESTDIR)/sbin
	install -m755 silo/silocheck $(DESTDIR)/usr/sbin
	[ -f $(DESTDIR)/etc/silo.conf ] || \
		install -m644 etc/silo.conf $(DESTDIR)/etc/
endif
ifeq ($(OPSYS),Linux)
ifneq ($(TILO_ONLY),yes)
	install -d -m755 $(DESTDIR)/boot
	install -m644 first/*.b second/*.b first-isofs/*.b $(DESTDIR)/boot/
endif
	install -d -m755 $(DESTDIR)/usr/bin
	install -m755 tilo/maketilo $(DESTDIR)/usr/bin/
	install -m755 tilo/tilo.sh $(DESTDIR)/usr/bin/tilo
endif
	for manpage in $(MANPAGES); do \
		sect=`echo $$manpage | sed 's/.*\([1-8]\)$$/\1/'`; \
		install -d -m755 $(DESTDIR)/usr/share/man/man$$sect; \
		install -m644 man/$$manpage $(DESTDIR)/usr/share/man/man$$sect/; \
	done
	if test x"$(DESTDIR)" = x; then \
		echo "You need to run 'silo -f' now, to update the boot block" 1>&2; \
	fi
else
install:
	@echo SILO is only supported on SPARC Linux and Solaris.
endif

tag:
	git-tag -s -m "Tag v$(VERSION)" v$(VERSION)

# This is just for me to make release tarballs
release: ../silo-$(VERSION).tar.gz ../silo-$(VERSION).tar.bz2
	rm -rf ../silo-$(VERSION) boot
# ../silo-loaders-$(VERSION).tar.gz

../silo-loaders-$(VERSION).tar%:
	rm -rf ../silo-$(VERSION)
	git-clone `pwd` ../silo-$(VERSION)
	(cd ../silo-$(VERSION); git-branch v$(VERSION) v$(VERSION); git-checkout v$(VERSION))
	rm -rf ../silo-$(VERSION)/.git
	install -d ../silo-$(VERSION)/boot
	for I in first first-isofs second; do $(MAKE) -C ../silo-$(VERSION)/$$I all || exit 1; done;
	install -d ../silo-$(VERSION)/boot
	(cd ../silo-$(VERSION); install -m644 first/*.b first-isofs/*.b second/*.b boot/)
	(cd ../silo-$(VERSION); \
		case "$*" in .gz) foo="gzip -c9";; .bz2) foo="bzip2 -c9";; *) foo=cat;; esac; \
		tar cf - boot | $$foo > $@; \
	)
	rm -rf ../silo-$(VERSION)

../silo-$(VERSION).tar%: clean
	rm -rf ../silo-$(VERSION)
	git-clone `pwd` ../silo-$(VERSION)
	(cd ../silo-$(VERSION); git-branch v$(VERSION) v$(VERSION); git-checkout v$(VERSION))
	rm -rf ../silo-$(VERSION)/.git
	case "$*" in .gz) foo="gzip -c9";; .bz2) foo="bzip2 -c9";; *) foo=cat;; esac; \
		(cd ../ && tar cf - silo-$(VERSION)) | $$foo > $@
	rm -rf ../silo-$(VERSION)
